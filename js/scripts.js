$(function() {

    var handler = function(e) {
        // e.preventDefault();
      switch(e.button) {
          case 1:
            console.log('Middle click');
            break;
        case 2:
            console.log('Right click');
            break;
        default:
            console.log('Click or Left click');
            break;
      }
    };
    $('#scrollHider *').on('contextmenu', handler);

    //mouse on drag item
    $(document).on("mouseover mouseout", '.dropped', function(e) {
        e.stopPropagation();
        if (e.type === "mouseover") {
            $(this).removeClass('item-hovered').addClass('item-hovered');
        } else {
            $('.item-hovered').each(function() {
                $(this).removeClass("item-hovered");
            });
        }
    });

    //clear selected outline
    // $("#centerPanel").on('click', function() {
    //     $('.item-selected').each(function() {
    //         $(this).removeClass('item-selected');
    //     });
    // });

    $(".deviceIcon").on('click', function() {
        $(".deviceIcon").removeClass('selected');
        var self = $(this);
        self.addClass('selected');
        var deviceScreen = $("#deviceScreen");
        deviceScreen.removeAttr('style');
        deviceScreen.removeClass();
        if(self.hasClass('web')) {
            deviceScreen.addClass("web");
        } else if(self.hasClass('iphoneX')) {
            deviceScreen.addClass("iphoneX");
        } else if(self.hasClass('tablet')) {
            deviceScreen.addClass("tablet");
        } else if(self.hasClass('ipad')) {
            deviceScreen.addClass("ipad");
        } else {
            deviceScreen.addClass("mobile");
        }
    });

    //drag item keyboard action
    var copy = null;
    var time = Date.now();
    $('*').keydown(function(e) {
        if ((Date.now() - time) < 200)  {
            time = Date.now();
        } else {
            time = Date.now();
            if(e.ctrlKey) {
                if(e.keyCode == 67) { //ctrl + c
                    copy = $('.item-selected');
                }
                if(e.keyCode == 86) { //ctrl + v
                    if(copy != null) {
                        var parent = $('.item-selected').parent();
                        if(parent) {
                            copy.each(function(e, element) {
                                var item = $(element);
                                var clone = item.clone();
                                clone.removeAttr('id');
                                dropElement(parent, clone);
                            });
                        }
                        refreshNestable();
                    }
                }
                if(e.keyCode == 66) { //ctrl + b
                }
            }
            if(e.keyCode == 46) { //delete
                $('.item-selected').remove();
                refreshNestable();
            }
        }
    });
    //init resizable
    makeResizable($("#deviceScreen"))

    //init droppable
    makeDroppable($('#dropArea'));

    //init draggable
    $('.dragItem').each(function(i, elem) {
        makeDraggable($(this), true, 'clone', 'new');
    });

    //select drag item
    $(document).on("click", '.dropped', function(e) {
        e.stopPropagation();
        $('.item-selected').each(function() {
            $(this).removeClass('item-selected');
            makeDraggable($(this), false);
        });
        $(this).addClass("item-selected");
        makeDraggable($(this));
        refreshNestable();
    });

    //select drag item from layer
    $(document).on("click", '.dd-item', function(e) {
        e.stopPropagation();
        $('.dd-item').removeClass('selected');
        $(this).addClass('selected');
        $('.item-selected').each(function() {
            $(this).removeClass('item-selected');
            makeDraggable($(this)), false;
        });
        var id = $(this).data('id');
        if (id) {
            $('#'+id).addClass("item-selected");
            makeDraggable($('#'+id));
        }
    });

    $("#centerPanel").mousedown(function(e) {
        // e.stopPropagation();
        div.hidden = 0;
        x1 = e.clientX - x5;
        y1 = e.clientY - y5;
        reCalc();
        mouseselect = true;
    });
    $("#centerPanel").mousemove(function(e) {
        x2 = e.clientX - x5;
        y2 = e.clientY - y5;
        reCalc();
    });
    $("#centerPanel").mouseup(function(e) {
        div.hidden = 1;
        reCalc(true);
        mouseselect = false;
    });
    refreshNestable();
});

function makeDraggable(elem, init = true, helper = 'original', lastid = '') {
    if (init) {
        elem.draggable({
            cursor: 'move',
            helper: helper,
            revert: "invalid",
            // stack: ".dragItem",
            // connectToSortable: ".dropArea",
            // snapTolerance: 30,
            start: function (event, ui) {
                ui.helper.data('dropped', true);
                dragging = true;
                if (lastid !== "") {
                    lastDrop = lastid;
                } else {
                    lastDrop = elem.attr('id');
                }
                $(ui.helper).addClass("dragging");
            },
            stop: function (event, ui) {
                dragging = false;
                elem.removeClass('dragging');
            }
        });
        elem.sortable({
            // axis: "y",
            revert: true,
            scroll: false
        });
    } else {
        try {
            elem.draggable("destroy");
        } catch(e) {
            // console.log("destroy uncessfully: ", e);
        }
    }
    // makeResizable($elem, init);
}
function makeResizable($elem, init = true) {
    if (init) {
        $elem.resizable({
            containment: "#centerPanel"
            // ghost: true,
            // autoHide: true
            // animate: true,
            // containment: $elem.parent().attr('id')
        });
    } else {
        try {
            $elem.resizable("destroy");
        } catch(e) {
            // console.log("destroy uncessfully: ", e);
        }
    }
}
var lastDrop = "";
function makeDroppable($elem, init = true) {
    if (init) {
        $elem.droppable({
            accept: ".dragItem",
            activeClass: "ui-state-highlight",
            hoverClass: "ui-state-hover",
            greedy: true,
            tolerance: 'touch',
            activate: function(e, ui) {
                e.stopPropagation();
            },
            over: function( event, ui ) {},
            drop: function(e, ui) {
                e.stopPropagation();
                if (lastDrop != "") {
                    dropElement($elem, ui);
                }
                lastDrop = "";
            }
        });
    } else {
        try {
            $elem.droppable("destroy");
        } catch(e) {
            // console.log("destroy uncessfully: ", e);
        }
    }
}
function dropElement($dropper, ui) {
    $(ui.draggable).removeClass('dragging');
    if (lastDrop !== 'new') { makeDraggable($(ui.draggable), false); }
    var item = $(ui.draggable).clone();
    // console.log(css(element)); //item styles
    if ($(ui.draggable).hasClass('dropped')) {
        $(ui.draggable).remove();
    }
    item.removeClass('dropped').addClass('dropped');
    item.uniqueId('div');
    item.find('*').each(function(i) {
        $(this).uniqueId();
    });

    item.appendTo($dropper);
    item.attr('data-id', item.attr('id').replace('ui-id-', ''));
    if ($(ui.draggable).hasClass('absolute')) {
        // var top = $(ui.helper).offset().top - $dropper.offset().top;
        item.css({'left': 0, 'top': 0});
    } else {
        item.css({'left': '', 'top': ''});
    }
    // item.css({'left': "", 'top': "", 'margin-left': left, 'margin-top': top});
    
    makeDroppableChild(item, "dropArea");
    refreshNestable();
}

function makeDroppableChild(element, name) {
    if (element.hasClass(name)) { makeDroppable(element); }
    if(element.children().length < 1) { return }
    else {
        element.children().each(function(i, elem) {
            makeDroppableChild($(elem), name);
        });
    }
}

var prevPositions = [];
var nestable = $('#nestable').nestable({
    group: 1,
    depth: 8,
    callback: function(l,e) {
        // l is the main container
        // e is the element that was moved
        // elementOrder = nestable.nestable('toArray')
        var id = e.data('id');
        var newPositions = nestable.nestable('toArray');
        var prevIndex = prevPositions.findIndex(x => x.id === id);
        var newIndex = newPositions.findIndex(x => x.id === id);
        var element = $('#'+id);
        if (element) {
            if (prevPositions[prevIndex].parent_id != newPositions[newIndex].parent_id) { //item parent changed
                if (newPositions[newIndex].parent_id != null) {
                    var preIndex = newPositions.findIndex(x => x.parent_id === newPositions[newIndex].parent_id);
                    if (newIndex - preIndex == 0) {
                        element.prependTo($('#' + newPositions[newIndex].parent_id));
                    } else {
                        element.insertAfter($('#' + newPositions[newIndex - 1].id));
                    }
                } else {
                    var grep = $.grep(newPositions, function(i) { return i.parent_id == null });
                    var newIndex = grep.findIndex(x => x.id === id);
                    if (newIndex == 0) {
                        element.prependTo(body);
                    } else {
                        element.insertAfter($('#' + grep[newIndex - 1].id));
                    }
                }
            } else if (prevIndex != newIndex) { //item position changed
                if (newPositions[newIndex].parent_id != null) {
                    var preIndex = newPositions.findIndex(x => x.parent_id === newPositions[newIndex].parent_id);
                    if (newIndex - preIndex == 0) {
                        element.prependTo($('#' + newPositions[newIndex].parent_id));
                    } else {
                        element.insertAfter($('#' + newPositions[newIndex - 1].id));
                    }
                } else {
                    var positionElement = element.parent().children().eq(newIndex);
                    if (newIndex < prevIndex) {
                        element.insertBefore(positionElement);
                    } else {
                        element.insertAfter(positionElement);
                    }
                }
            }
        }
    },
    beforeDragStop: function(l,e, p) {
    //     // l is the main container
    //     // e is the element that was moved
    //     // p is the place where element was moved.
    },
    onDragStart: function (l, e) {
        prevPositions = nestable.nestable('toArray');
    }
}).on('change', function(e) {
    var list   = e.length ? e : $(e.target);
    if (window.JSON) {
        $('#nestable-output').val(window.JSON.stringify(list.nestable('serialize')));//, null, 2));
    } else {
        console.log('JSON browser support required for this demo.');
    }
});

// function findChildDragItem222(element, name) {
//     if (element.hasClass(name)) { makeDroppable(element); }
//     if(element.children().length < 1) { return }
//     else {
//         element.children().each(function(i, elem) {
//             makeDroppableChild($(elem), name);
//         });
//     }
// }

// function space(count = 0) {var sp = "*";for(i=0;i<count;i++) { sp += "    "; } return sp;}
function loopChildren($elem, level = 0, html) {
    if($($elem).children().length < 1) {return;}
    list.push('<ol class="dd-list">');
    $($elem).children().each(function(i) {
        if ($(this).hasClass('xainar-layer')) {
            var children = "dd-nochildren";
            if ($(this).hasClass('ui-droppable')) { children = ""; }
            list.push('<li class="dd-item dd3-item '+ children +' ' + (($(this).hasClass('item-selected')) ? 'selected' : '') + '" data-id="'+$(this).get(0).id+'"><div class="dd-handle dd3-handle">Drag</div><div class="dd3-content">' + $(this).get(0).nodeName + ": " + $(this).get(0).id + '</div>');
            list.push('</li>');
        }
        loopChildren(this, level + 1, html);
    });
    list.push('</ol>');
}
var list = [];
function refreshNestable() {
    list = [];
    loopChildren($('#dropArea'), 0, "");
    $('#nestable').html(list.join(''));
    nestable.trigger('change');
}
//select area with mouse pointer
function selectItems(startX, startY, pageX, pageY) {
    $('.dropped').each(function(i, element) {
        var p = element.getBoundingClientRect();
        if (startX > pageX) {
            let x = startX;
            startX = pageX;
            pageX = x;
        }
        if (startY > pageY) {
            let y = startY;
            startY = pageY;
            pageY = y;
        }
        var select = false;
        if (((startX <= p.x && p.x <= pageX) || (startX <= (p.x + p.width) && (p.x + p.width) <= pageX))
            && ((startY <= p.y && p.y <= pageY) || (startY <= (p.y + p.height) && (p.y + p.height) <= pageY))) {
            select = true;
        }
        //  else {
        //     if (((p.x <= startX && startX <= p.x + p.width) || (p.x <= pageX && pageX <= p.x + p.width))
        //     && ((p.y <= startY && startY <= p.y + p.height) || (p.y <= pageY && pageY <= p.y + p.height))) {
        //         select = true
        //     }
        // }
        if (select) {
            $(element).addClass('item-selected');
            makeDraggable($(element));
        } else {
            $(element).removeClass('item-selected');
            makeDraggable($(element), false);
        }
        refreshNestable();
    });
}

var div = document.getElementById('selectArea'), x1 = 0, y1 = 0, x2 = 0, y2 = 0, x5 = document.getElementById('centerPanel').offsetLeft, y5 = document.getElementById('centerPanel').offsetTop, x6 = document.getElementById('centerPanel').offsetWidth, y6 = document.getElementById('centerPanel').offsetHeight;
var mouseselect = false;
var dragging = false;
function reCalc(mouseup = false) {
    if (mouseselect && !dragging) {
        var x3 = Math.min(x1,x2);
        var x4 = Math.max(x1,x2);
        var y3 = Math.min(y1,y2);
        var y4 = Math.max(y1,y2);
        div.style.left = x3 + 'px';
        div.style.top = y3 + 'px';
        div.style.width = x4 - x3 + 'px';
        div.style.height = y4 - y3 + 'px';
        if(mouseup) {
            selectItems(x3 + x5, y3 + y5, x4 + x5, y4 + y5);
        }
    } else {
        div.style.left = '0px';
        div.style.top = '0px';
        div.style.width = '0px';
        div.style.height = '0px';
    }
}

window.onbeforeunload = function(event) {
    console.log(event);
    // return confirm("Confirm refresh");
};
